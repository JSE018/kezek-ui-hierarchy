export interface ILocation {
  code?: string;
  nameRu?: string;
  nameKz?: string;
  parentCode?: string;
  showSublocations?: boolean;
  sublocations?: any[];
  timezone?: string;
}

export class Location implements ILocation {
  constructor(
    public code?: string,
    public nameRu?: string,
    public nameKz?: string,
    public parentCode?: string,
    public showSublocations?: boolean,
    public sublocations?: any[],
    public timezone?: string
  ) {}
}