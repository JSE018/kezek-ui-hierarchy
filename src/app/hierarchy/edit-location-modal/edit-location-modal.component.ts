import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ILocation, Location } from '../location.model';

@Component({
  selector: 'edit-location-modal',
  templateUrl: './edit-location-modal.component.html',
  styleUrls: [ './edit-location-modal.component.scss' ]
})
export class EditLocationModalComponent implements OnInit {
  private _sublocations: any[] = [];
  private _editMode = false;
  parentCodes: string[] = [
    "kezek-01",
    "kezek-02",
    "kezek-03"
  ];

  @Input()
  set location( location: ILocation ) {
    this._sublocations = location.sublocations;
    this._editMode = true;
    this.updateLocationForm( this.location );
  }

  @Input()
  set parentCode( parentCode: string ) {
    this.locationEditForm.patchValue({
      isSublocation: true,
      parentCode: parentCode
    });
  }

  locationEditForm = this.formBuilder.group({
    isSublocation: [ '' ],
    code: [ '', [ Validators.required, Validators.pattern( '^kezek-\\d+$' ) ] ],
    nameRu: [ '', [ Validators.required, Validators.maxLength( 50 ) ] ],
    nameKz: [ '', [ Validators.required, Validators.maxLength( 50 ) ] ],
    timezone: [ '', Validators.required ]
  });

  constructor( public activeModal: NgbActiveModal, private formBuilder: FormBuilder ) {}

  ngOnInit() {
    this.locationEditForm.get( 'isSublocation' ).valueChanges.subscribe( isSublocation => {
      if ( isSublocation ) {
        this.locationEditForm.addControl( 'parentCode', this.formBuilder.control( '', Validators.required ) );
      } else {
        this.locationEditForm.removeControl( 'parentCode' );
      }
    })
  }

  updateLocationForm( location: ILocation ) {
    if ( location.parentCode ) {
      this.locationEditForm.patchValue({
        parentCode: location.parentCode
      })
    }
    this.locationEditForm.patchValue({
      code: location.code,
      nameRu: location.nameRu,
      nameKz: location.nameKz,
      timezone: location.timezone
    });
  }

  passLocation() {
    let location = {
      ...new Location(),
      code: this.locationEditForm.get( 'code' ).value,
      nameRu: this.locationEditForm.get( 'nameRu' ).value,
      nameKz: this.locationEditForm.get( 'nameKz' ).value,
      showSublocations: false,
      sublocations: this._sublocations,
      timezone: this.locationEditForm.get( 'timezone' ).value
    }
    if ( this.locationEditForm.get(['isSublocation']).value ) {
      location = {
        ...location,
        parentCode: this.locationEditForm.get( 'parentCode' ).value,
      }
    }
    this.activeModal.close( location );
  }
}