import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { ILocation } from './location.model';
import { EditLocationModalComponent } from './edit-location-modal/edit-location-modal.component';
import { DeleteModalComponent } from '../layouts/delete-modal/delete-modal.component';

@Component({
  selector: 'app-hierarchy',
  templateUrl: './hierarchy.component.html',
  styleUrls: ['./hierarchy.component.scss']
})
export class HierarchyComponent implements OnInit {
  // locations: ILocation[] = [];
  locations: ILocation[] = [
    {
      code: "L1",
      nameRu: "L",
      nameKz: "L",
      showSublocations: false,
      sublocations: [
        {
          code: "LL1",
          nameRu: "L",
          nameKz: "L",
          parentCode: "L1"
        },
        {
          code: "LL2",
          nameRu: "L",
          nameKz: "L",
          parentCode: "L1"
        },
        {
          code: "LL3",
          nameRu: "L",
          nameKz: "L",
          parentCode: "L1"
        },
      ]
    },
    {
      code: "L2",
      nameRu: "L",
      nameKz: "L",
      showSublocations: false,
      sublocations: []
    },
    {
      code: "L3",
      nameRu: "L",
      nameKz: "L",
      showSublocations: false,
      sublocations: []
    },
  ];

  constructor(private modalService: NgbModal) {}

  ngOnInit() {}

  addLocation( parentCode?: string ) {
    const addLocationModal = this.modalService.open( EditLocationModalComponent );
    if ( parentCode ) {
      addLocationModal.componentInstance.parentCode = parentCode;
      addLocationModal.result.then( addedSublocation => {
        const parentIndex = this.locations.findIndex( s => s.code === addedSublocation.parentCode );
        this.locations[ parentIndex ].sublocations.push( addedSublocation );
      });
    } else {
      addLocationModal.result.then( addedLocation => {
        this.locations.push( addedLocation );
      });
    }
  }

  editLocation( locationToBeEdited: ILocation ) {
    const editLocationModal = this.modalService.open( EditLocationModalComponent );
    editLocationModal.componentInstance.location = locationToBeEdited;
    editLocationModal.result.then( editedLocation => {
      if ( editedLocation.parentCode ) {
        const parentIndex = this.locations.findIndex( s => s.code === editedLocation.parentCode );
        this.locations[ parentIndex ].sublocations.splice( this.locations.findIndex( s => s.code === editedLocation.code ), 1, editedLocation );
      } else {
        this.locations.splice( this.locations.findIndex( s => s.code === editedLocation.code ), 1, editedLocation );
      }
    });
  }

  deleteLocation( code, parentCode? ) {
    this.modalService.open( DeleteModalComponent ).result.then( result => {
      if ( result ) {
        if ( parentCode ) {
          const parentIndex = this.locations.findIndex( s => s.code === parentCode );
          this.locations[ parentIndex ].sublocations.splice( this.locations[ parentIndex ].sublocations.findIndex( s => s.code === code ), 1 );
        } else {
          this.locations.splice( this.locations.findIndex( s => s.code === code ), 1 );
        }
      }
    });
  }
}
