export interface ISettings {
	region?: string;
	branch?: string;
	displayedMedia?: string;
	panel?: string;
	file?: string;
}

export class Settings implements ISettings {
  constructor(
		public region?: string,
		public branch?: string,
		public displayedMedia?: string,
		public panel?: string,
		public file?: string
  ) {}
}