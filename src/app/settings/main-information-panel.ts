import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ISettings } from './settings.model';
import { EditSettingsModalComponent } from './edit-settings-modal/edit-settings-modal.component';
import { DeleteModalComponent } from '../layouts/delete-modal/delete-modal.component';
import { PlayControlModalComponent } from './play-control-modal/play-control-modal.component';

@Component({
	selector: 'main-information-panel',
	templateUrl: './main-information-panel.html',
	styleUrls: ['./main-information-panel.scss']
})
export class MainInformationPanelComponent implements OnInit {
	settings: ISettings[] = [
		{
			region: 'f',
			branch: 'f',
			displayedMedia: 'f',
			file:	'f'
		},
		{
			region: 'f',
			branch: 'f',
			displayedMedia: 'f',
			file:	'f'
		},
		{
			region: 'f',
			branch: 'f',
			displayedMedia: 'f',
			file:	'f'
		},
	];

	constructor(private modalService: NgbModal) {}

	ngOnInit() {}
	
	openPlayControl() {
		const playControlModal = this.modalService.open( PlayControlModalComponent );
	}

	addSettings() {
		const addSettingsModal = this.modalService.open( EditSettingsModalComponent );
	}

	editSettings(item) {
		const editSettingsModal = this.modalService.open( EditSettingsModalComponent );
	}
	
	deleteSettings() {
		this.modalService.open( DeleteModalComponent ).result.then( result => {
    });
	}
}