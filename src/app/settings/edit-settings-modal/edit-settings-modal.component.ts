import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ISettings, Settings } from '../settings.model';

@Component({
  selector: 'edit-settings-modal',
  templateUrl: './edit-settings-modal.component.html',
  styleUrls: [ './edit-settings-modal.component.scss' ]
})
export class EditSettingsModalComponent implements OnInit {
	regions: any[] = [];
	branches: any[] = [];
	panels: any[] = [];
	media: any[] = [];

	@Input()
  set settings( settings: ISettings ) {
    this.updateForm( this.settings );
  }

  constructor( public activeModal: NgbActiveModal, private formBuilder: FormBuilder ) {}

	ngOnInit() {}

	settingsEditForm = this.formBuilder.group({
    region: [ '', [ Validators.required ] ],
    branch: [ '', [ Validators.required ] ],
    displayedMedia: [ '', [ Validators.required ] ],
    panel: [ '', [ Validators.required ] ],
  });

	updateForm( settings: ISettings ) {
    this.settingsEditForm.patchValue({
      region: settings.region,
      branch: settings.branch,
			displayedMedia: settings.displayedMedia,
			panel: settings.panel,
    });
  }

	passSettings() {}
}