import { Component, OnInit } from '@angular/core';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'play-control-modal',
  templateUrl: './play-control-modal.component.html',
  styleUrls: [ './play-control-modal.component.scss' ]
})
export class PlayControlModalComponent implements OnInit {
	media: string[] = [
		"video1",
		"video2",
		"video3"
	];

  constructor( public activeModal: NgbActiveModal ) {}

	ngOnInit() {}

}