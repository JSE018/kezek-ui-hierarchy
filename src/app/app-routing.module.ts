import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HierarchyComponent } from './hierarchy/hierarchy.component';
import { LoginComponent } from './login/login.component';
import { BranchesComponent } from './branches/branches.component';
import { UsersComponent } from './users/users.component';
import { ServicePoolComponent } from './service-pool/service-pool.component';
import { AuthGuard } from './auth/auth.guard';
import { HomeComponent } from './home/home.component';
import { BranchConfigComponent } from './branches/branch-config/branch-config.component';
import { BranchInfoConfigComponent } from './branches/branch-config/branch-info-config.component';
import { BranchServicesConfigComponent } from './branches/branch-config/branch-services-config.component';
import { BranchDevicesConfigComponent } from './branches/branch-config/branch-devices-config.component';
import { MainInformationPanelComponent } from './settings/main-information-panel';


const routes: Routes = [
  {
    path: 'login',
    component: LoginComponent
  },  
  {
    path: '',
    canActivate: [AuthGuard],
    component: HomeComponent,
    children: [
			{
        path: 'branches',
        canActivateChild: [AuthGuard],
        component: BranchesComponent
			},
			{
        path: 'branch-config',
        canActivateChild: [AuthGuard],
        children: [
          {
            path: 'info',
            canActivateChild: [AuthGuard],
            component: BranchInfoConfigComponent
					},
					{
            path: 'services',
            canActivateChild: [AuthGuard],
            component: BranchServicesConfigComponent
          },
          {
            path: 'devices',
            canActivateChild: [AuthGuard],
            component: BranchDevicesConfigComponent
          }
        ],
        component: BranchConfigComponent
      },
      {
        path: 'hierarchy',
        canActivateChild: [AuthGuard],
        component: HierarchyComponent
			},
			{
        path: 'service-pool',
        canActivateChild: [AuthGuard],
        component: ServicePoolComponent
      },
      {
        path: 'users',
        canActivateChild: [AuthGuard],
        component: UsersComponent
			},
			{
        path: 'settings',
        canActivateChild: [AuthGuard],
        component: MainInformationPanelComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
