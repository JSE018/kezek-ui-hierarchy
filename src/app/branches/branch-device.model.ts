import { IService } from '../service-pool/service.model';

export interface IBranchDevice {
  autoCall?: boolean;
  type?: string;
  nameRu?: string;
  nameKz?: string;
  printTextRu?: string;
  printTextKz?: string;
  playSound?: boolean;
  sevenSegmentedDisplay?: boolean;
  showClientId?: boolean;
  soundDelay?: boolean;
  narrowPrintPaper?: boolean;
  operator?: number;
  operators?: number[];
  printPaperLength?: number;
  printQueueSize?: boolean;
  services?: IService;
  ticketCount?: number;
  desktopNotification?: string;
}

export class BranchDevice implements IBranchDevice {
  constructor(
    public autoCall?: boolean,
    public type?: string,
    public nameRu?: string,
    public nameKz?: string,
    public printTextRu?: string,
    public printTextKz?: string,
    public playSound?: boolean,
    public sevenSegmentedDisplay?: boolean,
    public showClientId?: boolean,
    public soundDelay?: boolean,
    public narrowPrintPaper?: boolean,
    public operator?: number,
    public operators?: number[],
    public printPaperLength?: number,
    public printQueueSize?: boolean,
    public services?: IService,
    public ticketCount?: number,
    public desktopNotification?: string
  ) {}
}