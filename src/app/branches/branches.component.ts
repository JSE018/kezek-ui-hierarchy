import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { BranchConfigService } from './branch-config/branch-config.service'
import { DeleteModalComponent } from '../layouts/delete-modal/delete-modal.component';
import { IBranch } from './branch.model';

@Component({
  selector: 'app-branches',
  host: {
    class: 'd-flex'
  },
  templateUrl: './branches.component.html',
  styleUrls: ['./branches.component.scss']
})
export class BranchesComponent implements OnInit {
  branches: IBranch[] = [
    {
      id: "1",
      nameRu: "f",
      nameKz: "f",
      addressRu: "f",
      addressKz: "f",
      branchCode: "f",
      timezone: "f",
      services: [],
      devices: []
    },
    {
      id: "1",
      nameRu: "f",
      nameKz: "f",
      addressRu: "f",
      addressKz: "f",
      branchCode: "f",
      timezone: "f",
      services: [],
      devices: []
    },
    {
      id: "1",
      nameRu: "f",
      nameKz: "f",
      addressRu: "f",
      addressKz: "f",
      branchCode: "f",
      timezone: "f",
      services: [],
      devices: []
    },
  ];

  constructor( private modalService: NgbModal, private branchConfigService: BranchConfigService ) { }

  ngOnInit() {}

  deleteBranch( id ) {
    this.modalService.open( DeleteModalComponent ).result.then( result => {
      if ( result ) {
        this.branches.splice( this.branches.findIndex( s => s.id === id ), 1 );
      }
    });
  }

  editBranch( branch ) {

  }

  addBranch() {
    
  }
}
