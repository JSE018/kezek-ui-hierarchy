import { Component, Input } from '@angular/core';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { IService } from 'src/app/service-pool/service.model';

@Component({
  selector: 'service-details-modal',
	templateUrl: './service-details-modal.component.html',
  styleUrls: [ './service-details-modal.component.scss' ]
})
export class ServiceDetailsModalComponent {
  private _service: IService;
  @Input()
  set service( serviceDetails: IService ) {
    this._service = serviceDetails;
  }

  constructor( public activeModal: NgbActiveModal ) {}
}