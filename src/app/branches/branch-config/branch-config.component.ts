import { Component, OnInit } from '@angular/core';

import { BranchConfigService } from './branch-config.service'
import { IBranch } from '../branch.model';

@Component({
  selector: 'app-branches',
  templateUrl: './branch-config.component.html',
  styleUrls: ['./branch-config.component.scss']
})
export class BranchConfigComponent implements OnInit {

  constructor(private branchConfigService: BranchConfigService) { }

  ngOnInit() {}

}
