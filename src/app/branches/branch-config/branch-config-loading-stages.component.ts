import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-branches-loading-stages',
  templateUrl: './branch-config-loading-stages.component.html',
  styleUrls: ['./branch-config-loading-stages.component.scss']
})
export class BranchConfigLoadingStagesComponent implements OnInit {
  @Input() loadingStage: number;

  constructor() {}

  ngOnInit() {}

}