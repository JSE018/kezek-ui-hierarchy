import { Component, OnInit }  from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router }             from '@angular/router';

import { BranchConfigService } from './branch-config.service'

@Component({
  selector: 'app-branches',
  templateUrl: './branch-info-config.component.html',
  styleUrls: ['./branch-info-config.component.scss']
})
export class BranchInfoConfigComponent implements OnInit {
  infoConfig: object;
  weekSchedule: any = {
    monday: [],
    tuesday: [],
    wednesday: [],
    thursday: [],
    friday: [],
    saturday: [],
    sunday: []
  };
  day: string = 'monday';

  infoEditForm = this.fb.group({
    nameRu: [ "", [ Validators.required, Validators.maxLength( 255 ) ] ],
    nameKz: [ "", [ Validators.required, Validators.maxLength( 255 ) ] ],
    addressRu: [ "", [ Validators.required, Validators.maxLength( 255 ) ] ],
    addressKz: [ "", [ Validators.required, Validators.maxLength( 255 ) ] ],
    branchCode: [ "", Validators.required ],
    banners: [ "", Validators.required ],
    timezone: [ "", Validators.required ],
  });

  constructor( private branchConfigService: BranchConfigService, public router: Router, private fb: FormBuilder ) { }

  ngOnInit() {
    this.branchConfigService.sharedInfoConfig.subscribe( infoConfig => this.infoConfig = infoConfig );
  }

  newInfoConfig() {
    this.infoConfig = {
      nameRu: this.infoEditForm.get([ 'nameRu' ]).value,
      nameKz: this.infoEditForm.get([ 'nameRu' ]).value,
      addressRu: this.infoEditForm.get([ 'nameRu' ]).value,
      addressKz: this.infoEditForm.get([ 'nameRu' ]).value,
      branchCode: this.infoEditForm.get([ 'nameRu' ]).value,
      weekSchedule: this.weekSchedule,
      banners: this.infoEditForm.get([ 'nameRu' ]).value,
      timezone: this.infoEditForm.get([ 'nameRu' ]).value,
    };
    this.branchConfigService.nextInfoConfig( this.infoConfig );
  }

  continueToServicesConfig() {
    // this.newInfoConfig();
    this.router.navigateByUrl('/branch-config/services');
  }
}
