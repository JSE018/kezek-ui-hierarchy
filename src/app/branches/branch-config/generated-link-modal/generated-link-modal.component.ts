import { Component, Input } from '@angular/core';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'generated-link-modal',
	templateUrl: './generated-link-modal.component.html',
  styleUrls: [ './generated-link-modal.component.scss' ]
})
export class GeneratedLinkModalComponent {
  @Input() public link: string;

  constructor( public activeModal: NgbActiveModal ) {}
}