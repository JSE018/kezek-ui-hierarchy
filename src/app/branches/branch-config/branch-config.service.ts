import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class BranchConfigService {
  private updatingBranch = new BehaviorSubject({});
  private updatedBranch = new BehaviorSubject({});
  private infoConfig = new BehaviorSubject({});
  private servicesConfig = new BehaviorSubject({});
  private devicesConfig = new BehaviorSubject([]);
  sharedInfoConfig = this.infoConfig.asObservable();
  sharedServicesConfig = this.servicesConfig.asObservable();
  sharedDevicesConfig = this.devicesConfig.asObservable();

  constructor() {}

  nextInfoConfig( infoConfigData: object ) {
    this.infoConfig.next( infoConfigData );
  }

  nextServicesConfig( servicesConfigData: object ) {
    this.servicesConfig.next( servicesConfigData );
  }

  nextDevicesConfig( devicesConfigData ) {
    this.devicesConfig.next( devicesConfigData );
  }
}
