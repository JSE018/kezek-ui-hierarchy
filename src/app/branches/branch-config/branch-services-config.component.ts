import { Component, OnInit }  from '@angular/core';
import { Router }             from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IService } from 'src/app/service-pool/service.model';
import { ICategory } from 'src/app/service-pool/category.model';
import { ServiceDetailsModalComponent } from './service-details-modal/service-details-modal.component';

@Component({
  selector: 'app-branches',
  templateUrl: './branch-services-config.component.html',
  styleUrls: ['./branch-services-config.component.scss']
})
export class BranchServicesConfigComponent implements OnInit {
  availableCategories: ICategory[] = [
		{
      nameRu: 'fffffff',
      nameKz: 'fffffff',
      showChildren: true,
      items: [
        {
          nameRu: 'f',
          nameKz: 'f',
          isHidden: true,
          services: [
            {
              id: 1,
              nameRu: 'f',
              nameKz: 'f',
              code: 'f',
              description: 'f',
              standard: 1,
              complexity: 1,
              priority: 1,
              callOrderable: true,
              color: 'f',
              type: 'service'
            }
          ],
          type: 'subcategory'
        },
      ]
    },
    {
      nameRu: 'fffffff',
      nameKz: 'fffffff',
      showChildren: true,
      items: []
    },
    {
      nameRu: 'fffffff',
      nameKz: 'fffffff',
      showChildren: true,
      items: []
    },
    {
      nameRu: 'fffffff',
      nameKz: 'fffffff',
      showChildren: true,
      items: []
    }
	];
  selectedCategories: ICategory[] = [
		{
      nameRu: 'fffffff',
      nameKz: 'fffffff',
      showChildren: true,
      items: [
        {
          nameRu: 'f',
          nameKz: 'f',
          isHidden: true,
          services: [
            {
              id: 1,
              nameRu: 'f',
              nameKz: 'f',
              code: 'f',
              description: 'f',
              standard: 1,
              complexity: 1,
              priority: 1,
              callOrderable: true,
              color: 'f',
              type: 'service'
            }
          ],
          type: 'subcategory'
        },
      ]
    },
    {
      nameRu: 'fffffff',
      nameKz: 'fffffff',
      showChildren: true,
      items: []
    },
    {
      nameRu: 'fffffff',
      nameKz: 'fffffff',
      showChildren: true,
      items: []
    },
    {
      nameRu: 'fffffff',
      nameKz: 'fffffff',
      showChildren: true,
      items: []
    }
	];
  servicesConfig: object;

  constructor( private modalService: NgbModal, public router: Router ) { }

  ngOnInit() {}

	seeDeviceInfo( service: any ) {
		const detailsModal = this.modalService.open( ServiceDetailsModalComponent );
    detailsModal.componentInstance.service = service;
	}

	deleteService() {
		
	}

  newServicesConfig() {}

  cancelConfig() {
    this.router.navigateByUrl('/branches');
  }

  continueToDevicesConfig() {
    this.newServicesConfig();
    this.router.navigateByUrl('/branch-config/devices');
  }
}