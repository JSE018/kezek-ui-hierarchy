import { Component, OnInit }  from '@angular/core';
import { FormBuilder, Validators, FormArray } from '@angular/forms';
import { Router }             from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IBranchDevice, BranchDevice } from '../branch-device.model';
import { DeleteModalComponent } from '../../layouts/delete-modal/delete-modal.component';

@Component({
  selector: 'app-branches',
  templateUrl: './branch-devices-config.component.html',
  styleUrls: ['./branch-devices-config.component.scss']
})
export class BranchDevicesConfigComponent implements OnInit {
  isConfigUpdated: boolean = false;
	availableServices: any[] = [];
	availableOperators: any[] = [];
	availableUsers: any[] = [];
  devices: IBranchDevice[] = [
		{
			nameRu: 'F',
			type: 'F',
		},
		{
			nameRu: 'F',
			type: 'F',
		},
		{
			nameRu: 'F',
			type: 'F',
		},
	];
  deviceType: string = '';
  updatedIndex: number;

  devicesEditForm = this.formBuilder.group({
    deviceType: ["", Validators.required],
    nameRu: ["", Validators.required],
    nameKz: ["", Validators.required],
  });

  constructor( private modalService: NgbModal, public router: Router, private formBuilder: FormBuilder ) { }
  
  ngOnInit() {
    this.devicesEditForm.get(['deviceType']).valueChanges.subscribe( selectedValue => {
      this.onDeviceTypeChanged( selectedValue );
    })
  }

  onDeviceTypeChanged( newDeviceType ) {
    this.deviceType = newDeviceType;
    switch( this.deviceType ) {
      case 'kiosk':
        this.devicesEditForm.addControl( 'printTextRu', this.formBuilder.control( '' ) );
        this.devicesEditForm.addControl( 'printTextKz', this.formBuilder.control( '' ) );
        this.devicesEditForm.addControl( 'playSound', this.formBuilder.control( '' ) );
        this.devicesEditForm.addControl( 'soundDelay', this.formBuilder.control( '' ) );
        this.devicesEditForm.addControl( 'narrowPrintPaper', this.formBuilder.control( '' ) );
        this.devicesEditForm.addControl( 'printPaperLength', this.formBuilder.control( '' ) );
        this.devicesEditForm.addControl( 'printQueueSize', this.formBuilder.control( '' ) );
        this.devicesEditForm.addControl( 'services', this.formBuilder.control( '', Validators.required ) );
        break;
      case 'panel':
        this.devicesEditForm.addControl( 'showCustomerId', this.formBuilder.control( '' ) );
        this.devicesEditForm.addControl( 'newsTickerText', this.formBuilder.control( '' ) );
        this.devicesEditForm.addControl( 'sevenSegmentDisplay', this.formBuilder.control( '' ) );
        this.devicesEditForm.addControl( 'ticketCount', this.formBuilder.control( '' ) );
        this.devicesEditForm.addControl( 'services', this.formBuilder.control( '', Validators.required ) );
        break;
      case 'window':
        this.devicesEditForm.addControl( 'windowId', this.formBuilder.control( '', Validators.required ) );
        this.devicesEditForm.addControl( 'autoCall', this.formBuilder.control( '', Validators.required ) );
        this.devicesEditForm.addControl( 'desktopNotification', this.formBuilder.control( '', Validators.required ) );
        this.devicesEditForm.addControl( 'services', this.formBuilder.control( '', Validators.required ) );
        this.devicesEditForm.addControl( 'users', this.formBuilder.control( ''	 ) );
        this.devicesEditForm.addControl( 'servicesPriority', this.formBuilder.control( '' ) );
        break;
      case 'display':
        this.devicesEditForm.addControl( 'operator', this.formBuilder.control( '', Validators.required ) );
        break;
      case 'parlour':
        this.devicesEditForm.addControl( 'operators', this.formBuilder.control( '', Validators.required ) );
        this.devicesEditForm.addControl( 'services', this.formBuilder.control( '', Validators.required ) );
        break;
      default:
        break;
    }
  }

  createKiosk() {
    const kiosk = {
      ...new BranchDevice(),
      deviceType: this.devicesEditForm.get(['deviceType']).value,
      nameRu: this.devicesEditForm.get(['nameRu']).value,
      nameKz: this.devicesEditForm.get(['nameKz']).value,
      printTextRu: this.devicesEditForm.get(['printTextRu']).value,
      printTextKz: this.devicesEditForm.get(['printTextKz']).value,
      playSound: this.devicesEditForm.get(['playSound']).value,
      soundDelay: this.devicesEditForm.get(['soundDelay']).value,
      narrowPrintPaper: this.devicesEditForm.get(['narrowPrintPaper']).value,
      printPaperLength: this.devicesEditForm.get(['printPaperLength']).value,
      printQueueSize: this.devicesEditForm.get(['printQueueSize']).value,
      services: this.devicesEditForm.get(['services']).value,
    }
    return kiosk;
  }

  createPanel() {
    const panel = {
      ...new BranchDevice(),
      deviceType: this.devicesEditForm.get(['deviceType']).value,
      nameRu: this.devicesEditForm.get(['nameRu']).value,
      nameKz: this.devicesEditForm.get(['nameKz']).value,
      showClientId: this.devicesEditForm.get(['showClientId']).value,
      sevenSegmentedDisplay: this.devicesEditForm.get(['sevenSegmentedDisplay']).value,
      ticketCount: this.devicesEditForm.get(['tokenCount']).value,
      services: this.devicesEditForm.get(['services']).value,
    }
    return panel;
  }

  createWindow() {
    const window = {
      ...new BranchDevice(),
      deviceType: this.devicesEditForm.get(['deviceType']).value,
      nameRu: this.devicesEditForm.get(['nameRu']).value,
      nameKz: this.devicesEditForm.get(['nameKz']).value,
      windowId: this.devicesEditForm.get(['windowId']).value,
      autoCall: this.devicesEditForm.get(['autoCall']).value,
      desktopNotification: this.devicesEditForm.get(['desktopNotification']).value,
      callUnservedCustomers: this.devicesEditForm.get(['callUnservedCustomers']).value,
      services: this.devicesEditForm.get(['services']).value,
      servicesPriority: this.devicesEditForm.get(['servicesPriority']).value,
    }
    return window;
  }

  createDisplay() {
    const display = {
      ...new BranchDevice(),
      deviceType: this.devicesEditForm.get(['deviceType']).value,
      nameRu: this.devicesEditForm.get(['nameRu']).value,
      nameKz: this.devicesEditForm.get(['nameKz']).value,
      operator: this.devicesEditForm.get(['operator']).value,
    }
    return display;
  }

  createParlour() {
    const parlour = {
      ...new BranchDevice(),
      deviceType: this.devicesEditForm.get(['deviceType']).value,
      nameRu: this.devicesEditForm.get(['nameRu']).value,
      nameKz: this.devicesEditForm.get(['nameKz']).value,
      operators: this.devicesEditForm.get(['operators']).value,
      services: this.devicesEditForm.get(['services']).value
    }
    return parlour;
  }

  startEdit( indexForUpdate ) {
    this.updatedIndex = indexForUpdate;
    this.fillUpdatingForms();
  }

  fillUpdatingForms() {
    switch( this.deviceType ) {
      case 'kiosk':
        break;
      case 'panel':
        break;
      case 'window':
        break;
      case 'display':
        break;
      case 'parlour':
        break;
      default:
        break;
    }
  }

  createOrUpdateDevice() {
    let device: IBranchDevice;
    switch( this.deviceType ) {
      case 'kiosk':
        device = this.createKiosk();
        break;
      case 'panel':
        device = this.createPanel();
        break;
      case 'window':
        device = this.createWindow();
        break;
      case 'display':
        device = this.createDisplay();
        break;
      case 'parlour':
        device = this.createParlour();
        break;
      default:
        break;
    }
    if ( device !== undefined ) {
      if ( this.updatedIndex !== undefined ) {
        this.devices.splice( this.updatedIndex, 1, device );
      } else {
        this.devices.push( device );
      }
      this.updatedIndex = undefined;
    }
  }

	editDevice() {

	}

  deleteDevice( name: string ) {
    this.modalService.open( DeleteModalComponent ).result.then( result => {
      if ( result ) {
        this.devices.splice( this.devices.findIndex( s => s.nameRu === name ), 1 );
      }
    });
  }

  passDevicesConfig() {}

  cancelConfig() {
    this.router.navigateByUrl('/branches');
  }

  finishConfig() {
    this.passDevicesConfig();
    this.router.navigateByUrl('/branches');
  }
}
