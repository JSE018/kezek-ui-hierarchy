import { IBranchDevice } from './branch-device.model';
import { IService } from '../service-pool/service.model';

export interface IBranch {
  id?: string;
  nameRu?: string;
  nameKz?: string;
  addressRu?: string;
  addressKz?: string;
  branchCode?: string;
  schedule?: any;
  timezone?: string;
  services?: IService[];
  devices?: IBranchDevice[];
}

export class Branch implements IBranch {
  constructor(
    public id?: string,
    public nameRu?: string,
    public nameKz?: string,
    public addressRu?: string,
    public addressKz?: string,
    public branchCode?: string,
    public schedule?: any,
    public timezone?: string,
    public services?: IService[],
    public devices?: IBranchDevice[]
  ) {}
}