import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../auth/auth.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { NotificationModalComponent } from '../notification-modal/notification-modal.component';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
	headerText = "Гродецкий Сергей [роль, филиал]";
	showNotificationToast = false;
	notificationCount: number = 4;
	notifications: any[] = [
		{
			'role': '[роль, филиал]',
			'text': 'Создан филиал "наименование филиала", для просмотра можете перейти по данной',
			'title': 'Гродецкий Сергей',
		},
		{
			'role': '[роль, филиал]',
			'text': 'Создан филиал "наименование филиала", для просмотра можете перейти по данной',
			'title': 'Гродецкий Сергей',
		},
		{
			'role': '[роль, филиал]',
			'text': 'Создан филиал "наименование филиала", для просмотра можете перейти по данной',
			'title': 'Гродецкий Сергей',
		},
	];
  constructor(public authService: AuthService, private modalService: NgbModal) {}

  ngOnInit() {}

	toggleShowNotificationToast():void {
		this.showNotificationToast = !this.showNotificationToast;
	}

	openNotificationModal():void {
		const notificationModal = this.modalService.open( NotificationModalComponent, { size: 'lg' } );
	}

  logout() {
    this.authService.logout();
  }
}