import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'notification-modal',
  templateUrl: './notification-modal.component.html',
  styleUrls: [ './notification-modal.component.scss' ]
})
export class NotificationModalComponent {
	notifications: any[] = [
		{
			'date': '19.12.2019 17:52:31',
			'role': '[роль, филиал]',
			'text': 'Создан филиал "наименование филиала", для просмотра можете перейти по данной ссылке',
			'title': 'Гродецкий Сергей',
		},
		{
			'date': '19.12.2019 17:52:31',
			'role': '[роль, филиал]',
			'text': 'Создан филиал "наименование филиала", для просмотра можете перейти по данной ссылке',
			'title': 'Гродецкий Сергей',
		},
		{
			'date': '19.12.2019 17:52:31',
			'role': '[роль, филиал]',
			'text': 'Создан филиал "наименование филиала", для просмотра можете перейти по данной ссылке',
			'title': 'Гродецкий Сергей',
		},
	];
	constructor( public activeModal: NgbActiveModal ) {}
	
	hideNotification( index ): void {
		this.notifications.splice( index, 1 );
	}
}