import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {
  sidebarOpen: boolean = false;
  monitoringOpen: boolean = false;
  reportsOpen: boolean = false;
  settingsOpen: boolean = false;
  administrationOpen: boolean = false;

  constructor() {}

  ngOnInit() {}

}