import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ICategory, Category } from '../category.model';

@Component({
  selector: 'edit-category-modal',
  templateUrl: './edit-category-modal.component.html',
  styleUrls: [ './edit-category-modal.component.scss' ]
})
export class EditCategoryModalComponent implements OnInit {
  private _items: any[];
  parentCategories: ICategory[] = [
    {

    },
    {

    }
  ];

  @Input()
  set category( category: ICategory ) {
    this._items = category.items;
    this.updateCategoryForm( this.category );
  }

  categoryEditForm = this.formBuilder.group({
    isSubcategory: [ '' ],
    nameRu: [ '', [ Validators.required, Validators.maxLength( 50 ) ] ],
    nameKz: [ '', [ Validators.required, Validators.maxLength( 50 ) ] ]
  });

  constructor( public activeModal: NgbActiveModal, private formBuilder: FormBuilder ) {}

  ngOnInit() {
    this.categoryEditForm.get( 'isSubcategory' ).valueChanges.subscribe( isSubcategory => {
      if ( isSubcategory ) {
        this.categoryEditForm.addControl( 'parentCategory', this.formBuilder.control( '', Validators.required ) );
      } else {
        this.categoryEditForm.removeControl( 'parentCategory' );
      }
    })
  }

  updateCategoryForm( category: ICategory ) {
    if ( category.parentCategory ) {
      this.categoryEditForm.patchValue({
        parentCategory: category.parentCategory
      });
    }
    this.categoryEditForm.patchValue({
      nameRu: category.nameRu,
      nameKz: category.nameKz,
    });
  }

  passCategory() {
    let category = {
      ...new Category(),
      nameRu: this.categoryEditForm.get( 'nameRu' ).value,
      nameKz: this.categoryEditForm.get( 'nameKz' ).value,
      showChildren: true,
      items: this._items,
      type: 'subcategory',
    }
    if ( this.categoryEditForm.get('isSubcategory').value ) {
      category = {
        ...category,
        parentCategory: this.categoryEditForm.get( 'parentCategory' ).value
      }
    }
    this.activeModal.close( category );
  }
}