export interface IService {
  nameRu?: string;
  nameKz?: string;
  code?: string;
  description?: string;
  rangeStart?: number;
  rangeEnd?: number;
  standard?: number;
  complexity?: number;
  priority?: number;
  callOrderable?: boolean;
  color?: string;
  type?: string;
}

export class Service implements IService {
  constructor(
    public nameRu?: string,
    public nameKz?: string,
    public code?: string,
    public description?: string,
    public rangeStart?: number,
    public rangeEnd?: number,
    public standard?: number,
    public complexity?: number,
    public priority?: number,
    public callOrderable?: boolean,
    public color?: string,
    public type?: string
  ) {}
}