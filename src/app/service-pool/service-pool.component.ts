import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IService } from './service.model';
import { EditServiceModalComponent } from './edit-service-modal/edit-service-modal.component';
import { DeleteModalComponent } from '../layouts/delete-modal/delete-modal.component';
import { EditCategoryModalComponent } from './edit-category-modal/edit-category-modal.component';
import { ICategory } from './category.model';

@Component({
  selector: 'app-service-pool',
  templateUrl: './service-pool.component.html',
  styleUrls: ['./service-pool.component.scss']
})
export class ServicePoolComponent implements OnInit {
  categories: ICategory[] = [
    {
      nameRu: 'fffffff',
      nameKz: 'fffffff',
      showChildren: false,
      items: [
        {
          nameRu: 'f',
          nameKz: 'f',
          showChildren: false,
          services: [
            {
              id: 1,
              nameRu: 'f',
              nameKz: 'f',
              code: 'f',
              description: 'f',
              standard: 1,
              complexity: 1,
              priority: 1,
              callOrderable: true,
              color: 'f',
              type: 'service'
            }
          ],
          type: 'subcategory'
        },
      ]
    },
    {
      nameRu: 'fffffff',
      nameKz: 'fffffff',
      showChildren: false,
      items: []
    },
    {
      nameRu: 'fffffff',
      nameKz: 'fffffff',
      showChildren: false,
      items: []
    },
    {
      nameRu: 'fffffff',
      nameKz: 'fffffff',
      showChildren: false,
      items: []
    }
  ];

  constructor( private modalService: NgbModal ) {}

  ngOnInit() {
  }

  addCategory() {
    const addCategoryModal = this.modalService.open( EditCategoryModalComponent );
    addCategoryModal.result.then( addedCategory => {
      if ( addedCategory.parentCategory ) {
        const parentIndex = this.categories.findIndex( s => s.nameRu === addedCategory.parentCategory.nameRu );
        this.categories[ parentIndex ].items.push( addedCategory );
      } else {
        this.categories.push( addedCategory );
      }
    });
  }

  addService( subcategoryName: string, categoryName: string ) {
    const addServiceModal = this.modalService.open( EditServiceModalComponent );
    addServiceModal.result.then( addedService => {
      const categoryIndex = this.categories.findIndex( s => s.nameRu === categoryName );
      const subcategoryIndex = this.categories[ categoryIndex ].items.findIndex( s => s.nameRu === subcategoryName );
      this.categories[ categoryIndex ].items[ subcategoryIndex ].services.push( addedService );
    });
  }

  editCategory( categoryToBeEdited: ICategory ) {
    const editCategoryModal = this.modalService.open( EditCategoryModalComponent );
    editCategoryModal.componentInstance.category = categoryToBeEdited;
    editCategoryModal.result.then( editedCategory => {
      if ( editedCategory.parentCategory ) {
        const parentIndex = this.categories.findIndex( s => s.nameRu === editedCategory.parentCategory.nameRu );
        this.categories[ parentIndex ].items.splice( this.categories[ parentIndex ].items.findIndex( s => s.nameRu === editedCategory.nameRu ), 1, editedCategory );
      } else {
        this.categories.splice( this.categories.findIndex( s => s.nameRu === editedCategory.nameRu ), 1, editedCategory );
      }
    });
  }

  editService( serviceToBeEdited: IService, subcategoryName: string, categoryName: string ) {
    const editServiceModal = this.modalService.open( EditServiceModalComponent );
    editServiceModal.componentInstance.service = serviceToBeEdited;
    editServiceModal.result.then( editedService => {
      const categoryIndex = this.categories.findIndex( s => s.nameRu === categoryName );
      const subcategoryIndex = this.categories[ categoryIndex ].items.findIndex( s => s.nameRu === subcategoryName );
      this.categories[ categoryIndex ].items[ subcategoryIndex ].services.splice( this.categories[ categoryIndex ].items[ subcategoryIndex ].services.findIndex( s => s.code === editedService.code ), 1, editedService );
    });
  }

  deleteService( code: string, subcategoryName: string, categoryName: string ) {
    this.modalService.open( DeleteModalComponent ).result.then( result => {
      if ( result ) {
        const categoryIndex = this.categories.findIndex( s => s.nameRu === categoryName );
        const subcategoryIndex = this.categories[ categoryIndex ].items.findIndex( s => s.nameRu === subcategoryName );
        this.categories[ categoryIndex ].items[ subcategoryIndex ].services.splice( this.categories[ categoryIndex ].items[ subcategoryIndex ].services.findIndex( s => s.code === code ), 1 );
      }
    });
  }

  deleteCategory( name: string, categoryName?: string ) {
    this.modalService.open( DeleteModalComponent ).result.then( result => {
      if ( result ) {
        if ( categoryName ) {
          const categoryIndex = this.categories.findIndex( s => s.nameRu === categoryName );
          this.categories[ categoryIndex ].items.splice( this.categories[ categoryIndex ].items.findIndex( s => s.nameRu === name ), 1 );
        } else {
          this.categories.splice( this.categories.findIndex( s => s.nameRu === name ), 1 );
        }
      }
    });
  }
}
