import { Component, Input } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { IService, Service } from '../service.model';

@Component({
  selector: 'edit-service-modal',
  templateUrl: './edit-service-modal.component.html',
  styleUrls: [ './edit-service-modal.component.scss' ]
})
export class EditServiceModalComponent {
  private _serviceToBeEdited: IService;

  @Input()
  set service( service: IService ) {
    this._serviceToBeEdited = service;
    this.updateForm( this._serviceToBeEdited );
  }

  serviceEditForm = this.formBuilder.group({
    nameRu: [ '', [ Validators.required, Validators.maxLength( 50 ) ] ],
    nameKz: [ '', [ Validators.required, Validators.maxLength( 50 ) ] ],
    code: [ '', [ Validators.required, Validators.pattern( '^[a-zA-Z0-9]{1,4}$' ) ] ],
    description: [ '', [ Validators.required, Validators.maxLength( 256 ) ] ],
    rangeStart: [ '', [ Validators.required, Validators.pattern( '^([1-9][0-9][0-9][0-9]|0[1-9][0-9][0-9]|00[1-9][0-9]|000[1-9])$' ) ] ],
    rangeEnd: [ '', [ Validators.required, Validators.pattern( '^([1-9][0-9][0-9][0-9]|0[1-9][0-9][0-9]|00[1-9][0-9]|000[1-9])$' ) ] ],
    standard: [ '', [ Validators.required, Validators.pattern( '^([1-9]|[1-9][0-9]|[1-9][0-9][0-9])$' ) ] ],
    complexity: [ '', [ Validators.required, Validators.pattern( '^[1-5]$' ) ] ],
    priority: [ '', [ Validators.required, Validators.pattern( '^[1-5]$' ) ] ],
    callOrderable: [ '', [ Validators.required ] ],
    color: [ '', [ Validators.required ] ],
  });

  constructor( public activeModal: NgbActiveModal, private formBuilder: FormBuilder ) {}

  updateForm( service: IService ) {
    this.serviceEditForm.patchValue({
      nameRu: service.nameRu,
      nameKz: service.nameKz,
      code: service.code,
      description: service.description,
      rangeStart: service.rangeStart,
      rangeEnd: service.rangeEnd,
      standard: service.standard,
      complexity: service.complexity,
      priority: service.priority,
      callOrderable: service.callOrderable,
      color: service.color
    });
  }

  passService() {
    const service = {
      ...new Service(),
      id: 1,
      nameRu: this.serviceEditForm.get(['name']).value,
      nameKz: this.serviceEditForm.get(['surname']).value,
      code: this.serviceEditForm.get(['login']).value,
      description: this.serviceEditForm.get(['email']).value,
      rangeStart: this.serviceEditForm.get(['rangeStart']).value,
      rangeEnd: this.serviceEditForm.get(['rangeEnd']).value,
      standard: this.serviceEditForm.get(['telephone']).value,
      complexity: this.serviceEditForm.get(['role']).value,
      priority: this.serviceEditForm.get(['code']).value,
      callOrderable: this.serviceEditForm.get(['code']).value,
      color: this.serviceEditForm.get(['code']).value,
    }
    this.activeModal.close( service );
  }
}