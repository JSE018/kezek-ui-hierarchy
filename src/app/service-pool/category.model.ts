export interface ICategory {
  nameRu?: string;
  nameKz?: string;
  showChildren?: boolean;
  parentCategory?: ICategory;
  items?: any[];
  type?: string;
}

export class Category implements ICategory {
  constructor(
    public nameRu?: string,
    public nameKz?: string,
    public showChildren?: boolean,
    public parentCategory?: ICategory,
    public items?: any[],
    public type?: string
  ) {}
}