import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ServicePoolComponent } from './service-pool.component';

describe('ServicePoolComponent', () => {
  let component: ServicePoolComponent;
  let fixture: ComponentFixture<ServicePoolComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ServicePoolComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ServicePoolComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
