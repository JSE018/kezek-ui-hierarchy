import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IUser } from './user.model';
import { DeleteModalComponent } from '../layouts/delete-modal/delete-modal.component';
import { EditUserModalComponent } from './edit-user-modal/edit-user-modal.component';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {
  // users: IUser[] = [];
  users: IUser[] = [
    {
      id: 1,
      name: 'Azamat',
      surname: 'Akhmetovich',
      loginStatus: 'user',
      email: 'test@mail.kz',
      telephone: '+77777777777',
      role: 'analyst',
      code: 'kezek-01',
      status: 'active'
    },
    {
      id: 2,
      name: 'Azamat',
      surname: 'Akhmetovich',
      loginStatus: 'user',
      email: 'test@mail.kz',
      telephone: '+77777777777',
      role: 'analyst',
      code: 'kezek-01',
      status: 'inactive'
    }
  ];

  constructor(private modalService: NgbModal) { }

  ngOnInit() {}

  addUser() {
    const addUserModal = this.modalService.open( EditUserModalComponent );
    addUserModal.result.then( addedUser => {
      this.users.push( addedUser );
    });
  }

  editUser( userToBeEdited: IUser ) {
    const editUserModal = this.modalService.open( EditUserModalComponent );
    editUserModal.componentInstance.user = userToBeEdited;
    editUserModal.result.then( editedUser => {
      this.users.splice( this.users.findIndex( s => s.id === editedUser.id ), 1, editedUser );
    });
  }
}
