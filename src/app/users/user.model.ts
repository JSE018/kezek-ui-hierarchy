export interface IUser {
  id?: number;
  name?: string;
  surname?: string;
  loginStatus?: string;
  email?: string;
  telephone?: string;
  company?: string;
  role?: string;
  code?: string;
  status?: string;
  schedule?: any;
}

export class User implements IUser {
  constructor(
    public id?: number,
    public name?: string,
    public surname?: string,
    public loginStatus?: string,
    public email?: string,
    public telephone?: string,
    public company?: string,
    public role?: string,
    public code?: string,
    public status?: string,
    public schedule?: any
  ) {}
}