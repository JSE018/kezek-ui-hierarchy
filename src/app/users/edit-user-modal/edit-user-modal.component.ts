import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { IUser, User } from '../user.model';


@Component({
  selector: 'edit-user-modal',
  templateUrl: './edit-user-modal.component.html',
  styleUrls: [ './edit-user-modal.component.scss' ]
})
export class EditUserModalComponent implements OnInit {
  weekSchedule: any = {
    monday: [],
    tuesday: [],
    wednesday: [],
    thursday: [],
    friday: [],
    saturday: [],
    sunday: []
  };

  @Input()
  set user( user: IUser ) {
    this.updateForm( this.user );
  }

  userEditForm = this.formBuilder.group({
    name: [ '', [ Validators.required, Validators.maxLength( 50 ) ] ],
    surname: [ '', [ Validators.required, Validators.maxLength( 50 ) ] ],
    telephone: [ '', [ Validators.required, Validators.maxLength( 255 ) ] ],
    company: [ '', [ Validators.required, Validators.maxLength( 255 ) ] ],
    role: [ '', [ Validators.required ] ],
    code: [ '', [ Validators.required, Validators.pattern( '^kezek-\\d{1,5}$' ) ] ],
    login: [ '', [ Validators.required, Validators.pattern( '^[a-zA-Z0-9]{6,20}$' ) ] ],
    email: [ '', [ Validators.required, Validators.email ] ],
    status: [ 'inactive', [ Validators.required ] ]
  });

  constructor( public activeModal: NgbActiveModal, private formBuilder: FormBuilder ) {}

  ngOnInit() {
    this.userEditForm.get( 'role' ).valueChanges.subscribe( role => {
      if ( role === 'Оператор' || role === 'Регистратор' ) {
        this.userEditForm.addControl( 'day', this.formBuilder.control( 'monday' ) );
        if ( this.userEditForm.contains( 'status' ) ) {
          this.userEditForm.removeControl( 'status' );
        }
      } else {
        this.userEditForm.removeControl( 'day' );
        if ( !this.userEditForm.contains( 'status' ) ) {
          this.userEditForm.addControl( 'status', this.formBuilder.control( 'monday', Validators.required ) )
        }
      }
    })
  }

  updateForm( user: IUser ) {
    this.userEditForm.patchValue({
      name: user.name,
      surname: user.surname,
      telephone: user.telephone,
      company: user.company,
      role: user.role,
      code: user.code,
      login: user.loginStatus,
      email: user.email,
    });
    if ( user.role === 'Оператор' || user.role === 'Регистратор' ) {
      this.weekSchedule = user.schedule;
    } else {
      this.userEditForm.patchValue({
        status: user.status
      })
    }
  }

  passUser() {
    let user = {
      ...new User(),
      id: 1,
      name: this.userEditForm.get(['name']).value,
      surname: this.userEditForm.get(['surname']).value,
      loginStatus: this.userEditForm.get(['login']).value,
      email: this.userEditForm.get(['email']).value,
      telephone: this.userEditForm.get(['telephone']).value,
      role: this.userEditForm.get(['role']).value,
      code: this.userEditForm.get(['code']).value,
    }
    if ( this.userEditForm.get( 'role' ).value === 'Оператор' || this.userEditForm.get( 'role' ).value === 'Регистратор' ) {
      user = {
        ...user,
        schedule: this.weekSchedule
      }
    } else {
      user = {
        ...user,
        status: this.userEditForm.get(['status']).value
      }
    }
    this.activeModal.close( user );
  }
}