import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HierarchyComponent } from './hierarchy/hierarchy.component';
import { LoginComponent } from './login/login.component';
import { ServicePoolComponent } from './service-pool/service-pool.component';
import { BranchesComponent } from './branches/branches.component';
import { BranchConfigComponent } from './branches/branch-config/branch-config.component';
import { BranchConfigLoadingStagesComponent } from './branches/branch-config/branch-config-loading-stages.component';
import { BranchInfoConfigComponent } from './branches/branch-config/branch-info-config.component';
import { BranchServicesConfigComponent } from './branches/branch-config/branch-services-config.component';
import { BranchDevicesConfigComponent } from './branches/branch-config/branch-devices-config.component';
import { UsersComponent } from './users/users.component';
import { HomeComponent } from './home/home.component';
import { NavbarComponent, SidebarComponent } from './layouts';
import { DeleteModalComponent } from './layouts/delete-modal/delete-modal.component';
import { EditCategoryModalComponent } from './service-pool/edit-category-modal/edit-category-modal.component';
import { EditLocationModalComponent } from './hierarchy/edit-location-modal/edit-location-modal.component';
import { EditUserModalComponent } from './users/edit-user-modal/edit-user-modal.component';
import { ServiceDetailsModalComponent } from './branches/branch-config/service-details-modal/service-details-modal.component';
import { GeneratedLinkModalComponent } from './branches/branch-config/generated-link-modal/generated-link-modal.component';
import { MainInformationPanelComponent } from './settings/main-information-panel';
import { EditSettingsModalComponent } from './settings/edit-settings-modal/edit-settings-modal.component';
import { PlayControlModalComponent } from './settings/play-control-modal/play-control-modal.component';
import { NotificationModalComponent } from './layouts/notification-modal/notification-modal.component';
import { EditServiceModalComponent } from './service-pool/edit-service-modal/edit-service-modal.component';

export function HttpLoaderFactory( httpClient: HttpClient ) {
  return new TranslateHttpLoader( httpClient );
}

@NgModule({
  declarations: [
    AppComponent,
    HierarchyComponent,
    HomeComponent,
		LoginComponent,
    ServicePoolComponent,
		BranchesComponent,
		BranchConfigComponent,
    BranchConfigLoadingStagesComponent,
		BranchInfoConfigComponent,
		BranchServicesConfigComponent,
    BranchDevicesConfigComponent,
    UsersComponent,
    NavbarComponent,
    SidebarComponent,
		DeleteModalComponent,
		EditCategoryModalComponent,
		EditServiceModalComponent,
    EditLocationModalComponent,
		EditUserModalComponent,
		ServiceDetailsModalComponent,
		GeneratedLinkModalComponent,
		MainInformationPanelComponent,
		EditSettingsModalComponent,
		PlayControlModalComponent,
		NotificationModalComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    NgbModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [ HttpClient ]
      }
    }),
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [ AppComponent ],
  entryComponents: [
		DeleteModalComponent,
    EditCategoryModalComponent,
		EditLocationModalComponent,
		EditServiceModalComponent,
		EditUserModalComponent,
		ServiceDetailsModalComponent,
		GeneratedLinkModalComponent,
		EditSettingsModalComponent,
		PlayControlModalComponent,
		NotificationModalComponent
  ]
})
export class AppModule { }